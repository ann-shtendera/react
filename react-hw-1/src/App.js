import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import './App.scss';
import Header from './components/Header/Header';
import AppRoutes from './AppRoutes';
import Modal from './components/Modal/Modal';
import { fetchProducts } from './redux/products/actionCreators';
import { fetchFavourites } from './redux/favourites/actionCreators';
import { fetchCartProducts } from './redux/cartProducts/actionCreators';

 
export default function App() {
	
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(fetchProducts());
		dispatch(fetchFavourites());
		dispatch(fetchCartProducts());
	}, [dispatch])


	return (
		<div className="App">
			<Header />
			<AppRoutes />
			<Modal />
		</div>
	);
}
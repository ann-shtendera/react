import { combineReducers } from 'redux';
import productsReducer from './products/reducer';
import favouritesReducer from './favourites/reducer';
import cartProductsReducer from './cartProducts/reducer';
import modalReducer from './modal/reducer';

const appReducer = combineReducers({
	products: productsReducer,
	favourites: favouritesReducer,
	cartProducts: cartProductsReducer,
	modal: modalReducer,
})

export default appReducer;
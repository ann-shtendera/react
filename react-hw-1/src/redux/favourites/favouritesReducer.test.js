import reducer from "./reducer";
import {addFavourite, setFavourites, deleteFavourite} from "./actionCreators";

const initialState = {
    items: [],
}
describe("favourites reducer tests", () => {

    it('should favourites reducer return initial state without args of state and action', () => {
        expect(reducer(undefined, {type: undefined})).toEqual(initialState);
    })


    it('should favourites reducer add products to state by SET_FAVOURITES action type', () => {
        const payloadMock = [{name: 'car', id: 2}];

        expect(reducer(undefined, setFavourites(payloadMock)))
            .toEqual({
                items: payloadMock,
            })
    })


    it('should favourites reducer add one new product by ADD_FAVOURITE action type', () => {
        const productMock = {name: 'ball', id: "3"}
        const stateMock = {
            items: [
                {name: 'car', id: "1"},
                {name: 'toy', id: "2"},
            ],
        };

        expect(reducer(stateMock, addFavourite(productMock)))
            .toEqual({
                items: [
                    {name: 'car', id: "1"},
                    {name: 'toy', id: "2"},
                    {name: 'ball', id: "3"},
                ]
            })
    })


    it('should favourites reducer delete one product by DELETE_FAVOURITE action type ', () => {
        const stateMock = {
            items: [
                {name: 'car', id: "1"},
                {name: 'toy', id: "2"},
            ],
        };

        expect(reducer(stateMock, deleteFavourite("1")))
            .toEqual({
                items: [
                    {name: 'toy', id: "2"},
                ]
            })
    })
})
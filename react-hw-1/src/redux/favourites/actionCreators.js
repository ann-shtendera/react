import { FAVOURITES_LS_KEY } from '../../constants';
import { getDataFromLocalStorage } from '../../utils/localStorageHelper';
import { ADD_FAVOURITE, DELETE_FAVOURITE, SET_FAVOURITES } from './actions';


export const setFavourites = (products) => ({ type: SET_FAVOURITES, payload: products});
export const deleteFavourite = (id) => ({ type: DELETE_FAVOURITE, payload: id});
export const addFavourite = (product) => ({ type: ADD_FAVOURITE, payload: product});


export const fetchFavourites = () => async (dispatch) => {
	try {
		const favourites = getDataFromLocalStorage(FAVOURITES_LS_KEY);
		
		if (favourites) {
			dispatch(setFavourites(favourites));
		} else {
			dispatch(setFavourites([]));
		}
		
	} catch(err) {
		console.log(err);
	}
}




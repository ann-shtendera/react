import { ADD_FAVOURITE, DELETE_FAVOURITE, SET_FAVOURITES } from "./actions";
import { setDataToLocalStorage } from './../../utils/localStorageHelper';
import { FAVOURITES_LS_KEY } from "../../constants";



const initialState = {
	items: [],
}

const favouritesReducer = (state = initialState, action) => {
	switch (action.type) {

		case SET_FAVOURITES: {
			return { ...state, items: action.payload };
		}

		case ADD_FAVOURITE: {
			const newItemsState = [...state.items, action.payload];
			setDataToLocalStorage(FAVOURITES_LS_KEY, newItemsState);
			return {...state, items: newItemsState };
		}

		case DELETE_FAVOURITE: {
			const newItemsState = state.items.filter(el => el.id !== action.payload);
			setDataToLocalStorage(FAVOURITES_LS_KEY, newItemsState);
			return {...state, items: newItemsState};
		}

		default: return state;
	}
}

export default favouritesReducer;
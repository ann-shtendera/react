import { CART_LS_KEY } from '../../constants';
import {
	getDataFromLocalStorage,
	removeDataFromLocalStorage,
} from '../../utils/localStorageHelper';
import {ADD_CART_PRODUCT, DELETE_ALL_CART_PRODUCTS, DELETE_CART_PRODUCT, SET_CART_PRODUCTS} from './actions';


export const setCartProducts = (products) => ({ type: SET_CART_PRODUCTS, payload: products});
export const deleteCartProduct = (id) => ({ type: DELETE_CART_PRODUCT, payload: id});
export const addCartProduct = (product) => ({ type: ADD_CART_PRODUCT, payload: product});
export const deleteAllCartProducts = () => ({type: DELETE_ALL_CART_PRODUCTS})

export const fetchCartProducts = () => async (dispatch) => {
	try {
		const cartProducts = getDataFromLocalStorage(CART_LS_KEY);
		
		if (cartProducts) {
			dispatch(setCartProducts(cartProducts));
		} else {
			dispatch(setCartProducts([]));
		}
		
	} catch(err) {
		console.log(err);
	}
}

export const fetchBuyCartProducts = (customerInfo) => async (dispatch) => {
	try {
		const cartProducts = getDataFromLocalStorage(CART_LS_KEY);
		console.log("Придбані товари: ", cartProducts);
		console.log("Інформація про замовника: ", customerInfo);

		if (cartProducts) {
			removeDataFromLocalStorage(CART_LS_KEY)
			dispatch(deleteAllCartProducts());
		}

	} catch(err) {
		console.log(err);
	}
}



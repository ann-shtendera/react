import {ADD_CART_PRODUCT, DELETE_ALL_CART_PRODUCTS, DELETE_CART_PRODUCT, SET_CART_PRODUCTS} from "./actions";
import {setDataToLocalStorage} from '../../utils/localStorageHelper';
import {CART_LS_KEY} from "../../constants";


const initialState = {
    items: [],
}

const cartProductsReducer = (state = initialState, action) => {
    switch (action.type) {

        case SET_CART_PRODUCTS: {
            return {...state, items: action.payload};
        }

        case ADD_CART_PRODUCT: {
            const index = state.items.findIndex(({id}) => id === action.payload.id);

            if (index === -1) {
                const newItemsState = [...state.items, {...action.payload, count: 1}];
                setDataToLocalStorage(CART_LS_KEY, newItemsState);
                return {...state, items: newItemsState};

            } else {
                const newItemsState = [...state.items];
                newItemsState[index].count++;
                setDataToLocalStorage(CART_LS_KEY, newItemsState);
                return {...state, items: newItemsState};
            }
        }

        case DELETE_CART_PRODUCT: {
            const newItemsState = state.items.filter(el => el.id !== action.payload);
            setDataToLocalStorage(CART_LS_KEY, newItemsState);
            return {...state, items: newItemsState};
        }

        case DELETE_ALL_CART_PRODUCTS: {
            return {...state, items: []};
        }

        default:
            return state;
    }
}

export default cartProductsReducer;
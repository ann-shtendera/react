import reducer from "./reducer";
import {addCartProduct, deleteAllCartProducts, deleteCartProduct, setCartProducts} from "./actionCreators";

const initialState = {
    items: [],
}
describe("cartProducts reducer tests", () => {

    it('should cartProducts reducer return initial state without args of state and action', () => {
        expect(reducer(undefined, {type: undefined})).toEqual(initialState);
    })

    it('should cartProducts reducer add products to state by SET_CART_PRODUCTS action type ', () => {
        const payloadMock = [{name: 'car', id: 2}];

        expect(reducer(undefined, setCartProducts(payloadMock)))
            .toEqual({
                items: payloadMock,
            })

    })

    it('should cartProducts reducer add one new product by ADD_CART_PRODUCT action type ', () => {
        const productMock = {name: 'ball', id: "3"}
        const stateMock = {
            items: [
                {name: 'car', id: "1", "count": 1},
                {name: 'toy', id: "2", "count": 1},
            ],
        };

        expect(reducer(stateMock, addCartProduct(productMock)))
            .toEqual({
                items: [
                    {name: 'car', id: "1", "count": 1},
                    {name: 'toy', id: "2", "count": 1},
                    {name: 'ball', id: "3", "count": 1},
                ]
            })
    })

    it('should cartProducts reducer increment count in product that exist by ADD_CART_PRODUCT action type', () => {
        const productMock = {name: 'toy', id: "2"}
        const stateMock = {
            items: [
                {name: 'car', id: "1", "count": 1},
                {name: 'toy', id: "2", "count": 1},
            ],
        };

        expect(reducer(stateMock, addCartProduct(productMock)))
            .toEqual({
                items: [
                    {name: 'car', id: "1", "count": 1},
                    {name: 'toy', id: "2", "count": 2},
                ]
            })
    })

    it('should cartProducts reducer delete one product by DELETE_CART_PRODUCT action type ', () => {
        const stateMock = {
            items: [
                {name: 'car', id: "1", "count": 1},
                {name: 'toy', id: "2", "count": 1},
            ],
        };

        expect(reducer(stateMock, deleteCartProduct("1")))
            .toEqual({
                items: [
                    {name: 'toy', id: "2", "count": 1},
                ]
            })
    })

    it('should cartProducts reducer delete all products by DELETE_ALL_CART_PRODUCTS action type ', () => {
        const stateMock = {
            items: [
                {name: 'car', id: "1", "count": 1},
                {name: 'toy', id: "2", "count": 1},
            ],
        };

        expect(reducer(stateMock, deleteAllCartProducts())).toEqual({items: []})
    })
})
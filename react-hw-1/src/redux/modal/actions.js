export const SET_MODAL = "SET_MODAL";
export const ADD_MODAL_TITLE = "ADD_MODAL_TITLE";
export const ADD_MODAL_BODY = "ADD_MODAL_BODY";
export const ADD_MODAL_ACTIONS = "ADD_MODAL_ACTIONS";
export const SET_MODAL_CLOSE_BTN = "SET_MODAL_CLOSE_BTN";

import { SET_MODAL, ADD_MODAL_BODY, ADD_MODAL_TITLE, ADD_MODAL_ACTIONS, SET_MODAL_CLOSE_BTN } from './actions';


export const setModalIsOpen = (value) => ({ type: SET_MODAL, payload: value});

export const addModalTitle = (value) => ({ type: ADD_MODAL_TITLE, payload: value});

export const addModalBody = (value) => ({ type: ADD_MODAL_BODY, payload: value});

export const addModalActions = (value) => ({ type: ADD_MODAL_ACTIONS, payload: value});

export const setModalCloseBtn = (value) => ({ type: SET_MODAL_CLOSE_BTN, payload: value});

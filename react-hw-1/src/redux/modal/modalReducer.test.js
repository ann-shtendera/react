import reducer from "./reducer";
import {setModalIsOpen, addModalTitle, addModalBody, addModalActions, setModalCloseBtn} from "./actionCreators";

const initialState = {
    isOpen: false,
    isCloseBtn: true,
    title: '',
    body: '',
    actions: null,
}
describe("Modal reducer tests", () => {

    it('should modal reducer return initial state without args of state and action', () => {
        expect(reducer(undefined, {type: undefined})).toEqual(initialState);
    })


    it('should modal reducer change isOpen by SET_MODAL action type', () => {
        expect(reducer(undefined, setModalIsOpen(true)))
            .toEqual({
                isOpen: true,
                isCloseBtn: true,
                title: '',
                body: '',
                actions: null,
            })
    })


    it('should modal reducer add title by ADD_MODAL_TITLE action type', () => {
        expect(reducer(undefined, addModalTitle('Hello world')))
            .toEqual({
                isOpen: false,
                isCloseBtn: true,
                title: "Hello world",
                body: '',
                actions: null,
            })
    })


    it('should modal reducer add body by ADD_MODAL_BODY action type', () => {
        expect(reducer(undefined, addModalBody('Delete this card?')))
            .toEqual({
                isOpen: false,
                isCloseBtn: true,
                title: '',
                body: 'Delete this card?',
                actions: null,
            })
    })


    it('should modal reducer add actions by ADD_MODAL_ACTIONS action type', () => {
        expect(reducer(undefined, addModalActions(<button>Click</button>)))
            .toEqual({
                isOpen: false,
                isCloseBtn: true,
                title: '',
                body: '',
                actions: <button>Click</button>,
            })
    })


    it('should modal reducer set close btn by SET_MODAL_CLOSE_BTN action type', () => {
        expect(reducer(undefined, setModalCloseBtn(false)))
            .toEqual({
                isOpen: false,
                isCloseBtn: false,
                title: '',
                body: '',
                actions: null,
            })
    })
})
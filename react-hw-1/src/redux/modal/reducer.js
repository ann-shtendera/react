import { ADD_MODAL_ACTIONS, ADD_MODAL_BODY, ADD_MODAL_TITLE, SET_MODAL, SET_MODAL_CLOSE_BTN } from "./actions";


const initialState = {
	isOpen: false,
	isCloseBtn: true,
	title: '',
	body: '',
	actions: null,
}

const modalReducer = (state = initialState, action) => {
	switch (action.type) {

		case SET_MODAL: {
			return { ...state, isOpen: action.payload };
		}

		case ADD_MODAL_TITLE: {
			return { ...state, title: action.payload };
		}

		case ADD_MODAL_BODY: {
			return { ...state, body: action.payload };
		}

		case ADD_MODAL_ACTIONS: {
			return { ...state, actions: action.payload };
		}

		case SET_MODAL_CLOSE_BTN: {
			return { ...state, isCloseBtn: action.payload };
		}

		default: return state;
	}
}

export default modalReducer;
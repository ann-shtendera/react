import reducer from "./reducer";
import {setProducts} from "./actionCreators";

jest.mock('axios', () => {});

const initialState = {
    items: [],
}
describe("Products reducer tests", () => {

    it('should products reducer return initial state without args of state and action', () => {
        expect(reducer(undefined, {type: undefined})).toEqual(initialState);
    })


    it('should products reducer add products to state by SET_PRODUCTS action type', () => {
        const payloadMock = [{name: 'car', id: 2}];

        expect(reducer(undefined, setProducts(payloadMock)))
            .toEqual({
                items: payloadMock,
            })
    })
})
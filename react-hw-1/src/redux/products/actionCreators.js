import axios from 'axios';
import { SET_PRODUCTS } from "./actions";


export const setProducts = (products) => ({ type: SET_PRODUCTS, payload: products});

export const fetchProducts = () => async (dispatch) => {
	try {
		const { data } = await axios.get('./catalog.json');
		dispatch(setProducts(data));
		
	} catch(err) {
		console.log(err);
	}
}




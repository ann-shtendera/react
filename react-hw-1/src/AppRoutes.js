import React from 'react';
import {Routes, Route} from 'react-router-dom';
import HomePage from './pages/HomePage/HomePage';
import CartPage from './pages/CartPage/CartPage';
import FavouritesPage from './pages/FavouritesPage/FavouritesPage';
import PageNotFound from './components/errorComponents/PageNotFound/PageNotFound';
import ProductsDisplayContextProvider from "./context/ProductsDisplayContext/ProductsDisplayContextProvider";



const AppRoutes = () => {
    return (
        <ProductsDisplayContextProvider>
            <Routes>
                <Route path='/' element={<HomePage/>}/>

                <Route path='/cart' element={<CartPage/>}/>

                <Route path='/favourites' element={<FavouritesPage/>}/>

                <Route path='*' element={<PageNotFound/>}/>
            </Routes>
        </ProductsDisplayContextProvider>
    );
}

export default AppRoutes;

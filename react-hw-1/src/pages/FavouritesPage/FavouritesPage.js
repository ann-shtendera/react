import React from 'react';
import styles from './FavouritesPage.module.scss';
import Product from '../../components/Product/Product';
import { useSelector } from 'react-redux';

const FavouritesPage = () => {

	const favourites = useSelector(state => state.favourites.items);

	return (
		<section className={styles.root}>
			{favourites?.length > 0 ? null : <h1>Товари в обраному відсутні</h1>}

			<div className={styles.container}>
				{favourites?.length > 0 && favourites.map(favourite => (
					<Product key={favourite.id}
						product={favourite}
					/>))}
			</div>

		</section>
	);
}

export default FavouritesPage;

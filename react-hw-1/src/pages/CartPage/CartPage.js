import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import styles from './CartPage.module.scss';
import Cart from '../../components/Cart/Cart';
import Button from '../../components/Button/Button';


const CartPage = () => {

	const navigate = useNavigate();

	const cartProducts = useSelector(state => state.cartProducts.items);

	if (!cartProducts.length) {
		return (
			<div className={styles.container}>
				<h1>Товари в корзині відсутні</h1>
				<Button bgColor={'#158aa1'} onClick={() => navigate('/')}>Додати товари</Button>
			</div>
		)
	}

	return (
		<div className={styles.container}>
			<Cart />
		</div>
	)
}


export default CartPage;

import React from 'react';
import ProductsContainer from '../../components/ProductsContainer/ProductsContainer';
import ProductsDisplaySwitcher from "../../components/ProductsDisplaySwitcher/ProductsDisplaySwitcher";

const HomePage = () => {

	return (
			<div>
				<ProductsDisplaySwitcher />
				<ProductsContainer />
			</div>
	);
}

export default HomePage;

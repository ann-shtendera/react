import ProductsDisplayContext from "./ProductsDisplayContext";
import {useState} from "react";

const ProductsDisplayContextProvider = ({ children }) => {

    const [displayType, setDisplayType] = useState('cards')

    const value = {
        displayType,
        setDisplayType,
    }

    return (
        <ProductsDisplayContext.Provider value={value}>
            {children}
        </ProductsDisplayContext.Provider>
    )
}

export default ProductsDisplayContextProvider;
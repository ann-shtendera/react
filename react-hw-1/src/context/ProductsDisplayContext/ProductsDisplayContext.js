import React, { createContext } from "react";

const ProductsDisplayContext = createContext({});

export default ProductsDisplayContext;
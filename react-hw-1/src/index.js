import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { ErrorBoundary } from "react-error-boundary";
import { BrowserRouter } from 'react-router-dom';
import './index.css';
import App from './App';
import SomethingWentWrong from './components/errorComponents/SomethingWentWrong/SomethingWentWrong';
import store from './redux/store';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
	<Provider store={store}>
		<ErrorBoundary fallback={<SomethingWentWrong />}>
			<BrowserRouter>
				<App />
			</BrowserRouter>
		</ErrorBoundary>
	</Provider>
);


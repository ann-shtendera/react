import React, {useContext} from "react";
import ProductsDisplayContext from "../../context/ProductsDisplayContext/ProductsDisplayContext";
import styles from './ProductsDisplaySwitcher.module.scss';
const ProductsDisplaySwitcher = () => {

    const { displayType, setDisplayType } = useContext(ProductsDisplayContext);

    return (
        <div className={styles.root}>
            <label htmlFor="displaySwitcher">Відобразити товари у вигляді: </label>
            <select
                name="displaySwitcher"
                id="displaySwitcher"
                value={displayType}
                onChange={({ target: { value } }) => setDisplayType(value)}
            >
                <option value='cards'>Карток</option>
                <option value='table'>Таблиці</option>
            </select>
        </div>
    )
}

export default ProductsDisplaySwitcher;
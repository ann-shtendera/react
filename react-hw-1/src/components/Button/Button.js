import React, {memo} from 'react';
import styles from './Button.module.scss';
import classNames from 'classnames';
import PropTypes from 'prop-types';


const Button = ({bgColor, onClick, children, type, disabled, size, classes}) => {

    if (size === 'small') {
        return (<button className={classes ? classes : classNames(styles.btn, styles.small)}
                        type={type}
                        style={{backgroundColor: disabled ? '' : bgColor}}
                        onClick={onClick}
                        disabled={disabled}
            >
                {children}
            </button>
        )
    }

    return (<button className={classes ? classes : styles.btn}
                    type={type}
                    style={{backgroundColor: disabled ? '' : bgColor}}
                    onClick={onClick}
                    disabled={disabled}
        >
            {children}
        </button>
    );
}

Button.propTypes = {
    bgColor: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.arrayOf(PropTypes.node),
    ]),
    type: PropTypes.string,
    disabled: PropTypes.bool,
    size: PropTypes.string,
    classes: PropTypes.string
}

Button.defaultProps = {
    bgColor: '',
    onClick: () => {},
    size: '',
    type: 'button',
    children: '',
    disabled: false,
    classes: '',
}

export default memo(Button);

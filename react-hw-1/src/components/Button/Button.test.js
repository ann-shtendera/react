import Button from "./Button";
import {fireEvent, render} from "@testing-library/react";

describe('smoke test for Button (Snapshot)', () => {
    it('should render Button', () => {
        const {asFragment} = render(
            <Button bgColor='#3c3c3c'
                    type='button'
                    disabled={false}
                    size='small'
                    classes='styles.formBtn'>
                Click me!
            </Button>);

        expect(asFragment()).toMatchSnapshot();
    })
});

describe('Button click', () => {
    const handleClickMock = jest.fn();

    it('should Button call fn onClick', () => {

        const { getByRole } = render(
            <Button onClick={handleClickMock}
                    bgColor='#3c3c3c'
                    classes='styles.formBtn'>
                Click me!
            </Button>
        )
        const btn = getByRole('button');

        fireEvent.click(btn);

        expect(handleClickMock).toHaveBeenCalledTimes(1);
    })
});
import React from 'react';
import { useSelector } from 'react-redux';
import { Link, NavLink } from 'react-router-dom';
import styles from './Header.module.scss';
import StarFavourite from '../../assets/svgComponents/StarFavourite';
import CartIcon from '../../assets/svgComponents/CartIcon';
import classNames from 'classnames';


const Header = () => {

	const cartProducts = useSelector(state => state.cartProducts.items);
	const favourites = useSelector(state => state.favourites.items);

	const cartAmount = cartProducts.reduce((acc, cur) => acc + cur.count, 0);
	const favouritesAmount = favourites.length;
	
	return (
		<div className={styles.header}>
			<div className={styles.container}>
				<Link to="/" className={styles.logo}>
					<img src="./img/svg/castle-svgrepo-com.svg" alt="castle-logo" />
					<span>CastlePlay</span>
				</Link>

				<nav>
					<ul className={styles.navigation}>
						<li>
							<NavLink to="/" className={ ({ isActive }) => classNames( {[styles.active]: isActive} )}>Головна сторінка</NavLink>
						</li>
						<li>
							<NavLink to="/cart" className={ ({ isActive }) => classNames( {[styles.active]: isActive} )}>Корзина</NavLink>
						</li>
						<li>
							<NavLink to='/favourites' className={ ({ isActive }) => classNames( {[styles.active]: isActive} )}>Обране</NavLink>
						</li>
					</ul>
				</nav>

				<div className={styles.iconsWrapper}>
					<Link to='/cart' className={styles.cartContainer}>
						<CartIcon />
						<span className={styles.cartCounts}>{cartAmount}</span>
					</Link>

					<Link to='/favourites' className={styles.favouritesContainer}>
						<StarFavourite fill="#ddd30e" width='26' height='26' />
						<span className={styles.favouriteCounts}>{favouritesAmount}</span>
					</Link>
				</div>

			</div>
		</div>
	);
}


export default Header;

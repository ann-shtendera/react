import React, {useContext, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import styles from './Product.module.scss';
import Button from '../Button/Button';
import StarFavourite from '../../assets/svgComponents/StarFavourite';
import {addFavourite, deleteFavourite} from '../../redux/favourites/actionCreators';
import {addCartProduct} from '../../redux/cartProducts/actionCreators';
import {
    addModalActions,
    addModalBody,
    addModalTitle,
    setModalCloseBtn,
    setModalIsOpen
} from '../../redux/modal/actionCreators';
import ProductsDisplayContext from "../../context/ProductsDisplayContext/ProductsDisplayContext";
import CartIcon from "../../assets/svgComponents/CartIcon";
import classNames from "classnames";


const Product = ({product}) => {
    const {id, name, url, article, price} = product;

    const {displayType} = useContext(ProductsDisplayContext);

    const dispatch = useDispatch();

    const favourites = useSelector(state => state.favourites.items);

    const [isFavourite, setIsFavourite] = useState(checkIsFavourite);

    function checkIsFavourite() {
        const favourItem = favourites.find(item => item.id === id);
        return favourItem ? true : false;
    }

    const handleAddToFavourites = () => {
        if (isFavourite) {
            setIsFavourite(false);
            dispatch(deleteFavourite(id));

        } else {
            setIsFavourite(true);
            dispatch(addFavourite(product));
        }
    }

    const handleAddToCart = () => {
        dispatch(addCartProduct(product));
    }

    const handleOpenModal = () => {
        dispatch(setModalCloseBtn(true));
        dispatch(addModalTitle('Додати товар в корзину'));
        dispatch(addModalBody(`Ви дійсно хочете додати "${name}" в корзину?`));
        dispatch(addModalActions(<>
            <Button bgColor={'#158aa1'} onClick={() => {
                handleAddToCart();
                dispatch(setModalIsOpen(false))
            }}>
                Додати
            </Button>
            <Button bgColor={'#158aa1'} onClick={() => dispatch(setModalIsOpen(false))}>
                Скасувати
            </Button>
        </>));
        dispatch(setModalIsOpen(true));
    }

    if (displayType === 'cards') {
        return (
            <div className={styles.card}>
                <Button classes={styles.star} onClick={handleAddToFavourites}>
                    <StarFavourite fill={isFavourite ? '#ddd30e' : '#8ec9cd'} width='19' height='19'/>
                </Button>

                <a href="#" className={styles.imgWrapper}>
                    <img src={url} alt={name} className={styles.img}/>
                </a>

                <div className={styles.decription}>
                    <a href="#" className={classNames(styles.cardsName, styles.name)}>{name}</a>
                    <span className={styles.article}>Код: {article}</span>
                </div>

                <div className={styles.cardFooter}>
                    <span className={styles.price}>{price} грн.</span>
                    <Button onClick={handleOpenModal} bgColor="#158aa1" size='small'>Купити</Button>
                </div>
            </div>
        );
    }

    if (displayType === 'table') {
        return (
            <tr className={styles.tableRow} key={id}>
                <td>
                    <a href="#">
                        <img src={url} alt={name} width="150" height="150"/>
                    </a>
                </td>
                <td>
                    <a href="#">{name}</a>
                </td>
                <td>
                    <p className={styles.tableArticle}>{article}</p>
                </td>
                <td className={styles.price}>{price} грн</td>
                <td>
                    <div className={styles.tableButtons}>
                        <Button onClick={handleAddToFavourites} bgColor={'transparent'}>
                            <StarFavourite fill={isFavourite ? '#ddd30e' : '#8ec9cd'} width='25' height='25'/>
                        </Button>
                        <Button onClick={handleOpenModal} bgColor="#158aa1" size='small'>
                            <CartIcon/>
                        </Button>
                    </div>

                </td>
            </tr>
        )
    }
}

Product.propTypes = {
    product: PropTypes.object,
}

Product.defaultProps = {
    product: {},
}

export default Product;

import React from 'react';
import {Form, Formik} from "formik";
import validationSchema from "./validationSchema";
import Button from "../../Button/Button";
import Input from "../../Input/Input";
import styles from './PurchaseForm.module.scss';
import {useDispatch} from "react-redux";
import {fetchBuyCartProducts} from "../../../redux/cartProducts/actionCreators";

const PurchaseForm = () => {

    const dispatch = useDispatch();

    const initialValues = {
        firstName: '',
        lastName: '',
        age: '',
        deliveryAddress: '',
        phoneNumber: '',
        email: '',
    }

    const onSubmit = (values, actions) => {
        dispatch(fetchBuyCartProducts(values))
        actions.resetForm();
    }

    return (
        <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            validationSchema={validationSchema}
        >
            {({isValid}) => (
                <Form className={styles.root}>
                    <h3 className={styles.checkoutTitle}>Оформлення замовлення</h3>
                    <Input name="firstName" id="firstName" label="Ім'я*"/>
                    <Input name="lastName" id="lastName" label="Прізвище*"/>
                    <Input name="age" id="age" label="Вік*"/>
                    <Input name="deliveryAddress" id="deliveryAddress" label="Адреса доставки*"/>
                    <Input name="email" id="email" label="Электронна пошта*"/>
                    <Input type='tel' name="phoneNumber" label="Мобільний телефон*"/>

                    <p className={styles.note}>* Поля обов'язкові для заповнення</p>
                    <Button bgColor='#158aa1'
                            type='submit'
                            disabled={!isValid}
                    >
                        Підтвердити замовлення
                    </Button>
                </Form>
            )}
        </Formik>
    );
}

export default PurchaseForm;

import React from 'react';
import {useField} from "formik";
import styles from './Input.module.scss';
import PropTypes from "prop-types";
import {PatternFormat} from "react-number-format";

function Input({label, type, ...props}) {

    const [field, meta] = useField(props.name);

    return (
        <div className={styles.root}>
            {label && <label htmlFor={props.id}>{label}</label>}
            {type === 'tel'
                ? <PatternFormat format="+38 (###) ###-####" mask="_" {...field} {...props} />
                : <input type={type} {...field} {...props}/>
            }
            {meta.error && meta.touched && <div className={styles.error}>{meta.error}</div>}
        </div>
    )
}

Input.propTypes = {
    label: PropTypes.string,
    type: PropTypes.string,
};

Input.defaultProps = {
    label: "",
    type: 'text',
}
export default Input;
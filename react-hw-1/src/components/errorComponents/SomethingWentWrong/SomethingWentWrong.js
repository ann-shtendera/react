import React from 'react';
import styles from './SomethingWentWrong.module.scss';
import Button from '../../Button/Button';


const SomethingWentWrong = () => {

	const handleReload = () => {
		document.location.reload();
	}
 	return (
		<div className={styles.root}>
			<p>Упс... Щось пішло не так.</p>
			<p>Спробуйте перезавантажити сторінку.</p>
			<Button bgColor={"#338674"} onClick={handleReload}>Reload</Button>
		</div>
	);
}

export default SomethingWentWrong;

import SomethingWentWrong from "./SomethingWentWrong";
import {render} from "@testing-library/react";

describe('smoke test for SomethingWentWrong (Snapshot)', () => {
    it('should render SomethingWentWrong', () => {
        const {asFragment} = render(
            <SomethingWentWrong />);

        expect(asFragment()).toMatchSnapshot();
    })
});
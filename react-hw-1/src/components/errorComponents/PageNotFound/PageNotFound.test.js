import PageNotFound from "./PageNotFound";
import {render} from "@testing-library/react";

describe('smoke test for PageNotFound (Snapshot)', () => {
    it('should render PageNotFound', () => {
        const {asFragment} = render(
            <PageNotFound />);

        expect(asFragment()).toMatchSnapshot();
    })
});
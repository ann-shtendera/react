import React from 'react';
import styles from './PageNotFound.module.scss';


const PageNotFound = () => {

 	return (
		<div className={styles.root}>
			<h1>404</h1>
			<p>Сторінка не знайдена</p>
		</div>
	);
}

export default PageNotFound;

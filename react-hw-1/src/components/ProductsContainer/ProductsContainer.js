import React, {useContext} from 'react';
import {useSelector} from 'react-redux';
import styles from './ProductsContainer.module.scss';
import Product from '../Product/Product';
import ProductsDisplayContext from "../../context/ProductsDisplayContext/ProductsDisplayContext";
import classNames from "classnames";


const ProductsContainer = () => {

    const {displayType} = useContext(ProductsDisplayContext);

    const products = useSelector(state => state.products.items);

    if (displayType === 'cards') {
        return (
            <section className={classNames(styles.container, styles.cardsContainer)}>
                {products?.length > 0 && products.map(product => (
                    <Product
                        key={product.id}
                        product={product}
                    />))
                }
            </section>
        )
    }

    if (displayType === 'table') {
        return (
            <section className={styles.container}>

                <table className={styles.productsTable}>
                    <thead>
                    <tr>
                        <th>Фото</th>
                        <th>Назва товару</th>
                        <th>Артикул</th>
                        <th>Ціна</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {products?.length > 0 && products.map(product => (
                        <Product
                            key={product.id}
                            product={product}
                        />))
                    }
                    </tbody>
                </table>
            </section>
        )
    }
}

    export default ProductsContainer;

import { useEffect } from "react";
import { Provider, useDispatch } from "react-redux";
import { render, fireEvent, screen } from '@testing-library/react';
import Modal from './Modal';
import {addModalActions, setModalIsOpen} from "../../redux/modal/actionCreators";
import store from "../../redux/store";


const Dispatcher = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(setModalIsOpen(true))
        dispatch(addModalActions(<button onClick={()=>{}}>Add to cart</button>))
    }, [])

    return null;
}

const Component = () => {
    return (
        <Provider store={store}>
            <Dispatcher />
            <Modal />
        </Provider>
    )
}

describe('Smoke test (Snapshot test)', () => {
    it('should Modal render', () => {
        const { asFragment } = render(<Component />);

        expect(asFragment()).toMatchSnapshot();
    });
});

describe('Functions work', () => {

    it('should Modal close on close background click', () => {
        render(<Component />);

        fireEvent.click(screen.getByTestId('background'));

        expect(screen.queryByTestId('modal')).toBeNull();
    });

    it('should Modal close on close btn click', () => {
        render(<Component />);

        fireEvent.click(screen.getByTestId('btnClose'));

        expect(screen.queryByTestId('modal')).toBeNull();
    });
});
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styles from './Modal.module.scss';
import { setModalIsOpen } from '../../redux/modal/actionCreators';
import Button from '../Button/Button';



const Modal = () => {

	const { isOpen, isCloseBtn, title, body, actions } = useSelector(state => state.modal);

	const dispatch = useDispatch();

	const handleCloseModal = () => {
		dispatch(setModalIsOpen(false));
	}

	if (!isOpen) return null;
	
	return (
		<div className={styles.wrapper} data-testid='modal'>
			<div className={styles.background} onClick={handleCloseModal} data-testid='background'></div>
			<div className={styles.modal}>
				<div className={styles.header}>
					<span>
						{title}
					</span>
					{isCloseBtn && <button data-testid='btnClose' className={styles.closeBtn} onClick={handleCloseModal}></button>}
				</div>
				<div className={styles.mainContainer}>
					{body}
				</div>
				{actions && <div className={styles.btnsWrapper}>
					{actions}
				</div>}
			</div>
		</div>
	);
}


export default Modal;

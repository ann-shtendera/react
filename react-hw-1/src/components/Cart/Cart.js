import React, {memo} from 'react';
import {useSelector} from 'react-redux';
import styles from './Cart.module.scss';
import CartItem from '../CartItem/CartItem';
import PurchaseForm from "../forms/PurchaseForm/PurchaseForm";


const Cart = () => {

    const cartProducts = useSelector(state => state.cartProducts.items);

    const getTotalSum = () => cartProducts.reduce((acc, cur) => acc + (cur.count * cur.price), 0);

    return (
        <>
            <section className={styles.cartWrapper}>

                <div>
                    <h3 className={styles.cartTitle}>Товари в корзині</h3>

                    <table className={styles.cartTable}>
                        <thead>
                        <tr>
                            <th>Фото</th>
                            <th>Назва товару</th>
                            <th>Ціна</th>
                            <th>Кількість</th>
                            <th>Сума</th>
                            <th>Видалити</th>
                        </tr>
                        </thead>
                        <tbody>

                        {cartProducts.map(({id, name, article, price, url, count}) => {
                            return <CartItem key={id} id={id} name={name}
                                             article={article} price={price}
                                             url={url} count={count}/>
                        })}
                        </tbody>
                    </table>

                    <div className={styles.totalSum}>
                        <p>{`До оплати: ${getTotalSum()} грн.`}</p>
                    </div>

                </div>

                <div className={styles.form}>
                    {/*<h3>Оформлення замовлення</h3>*/}
					<PurchaseForm/>
                </div>

            </section>
        </>
    );
}

export default memo(Cart);

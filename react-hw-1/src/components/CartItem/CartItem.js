import React, { memo } from 'react';
import { useDispatch } from 'react-redux';
import styles from './CartItem.module.scss';
import DeleteIcon from '../../assets/svgComponents/DeleteIcon';
import Button from '../Button/Button';
import { deleteCartProduct } from '../../redux/cartProducts/actionCreators';
import { addModalActions, addModalBody, addModalTitle, setModalCloseBtn, setModalIsOpen } from '../../redux/modal/actionCreators';


const CartItem = ({ id, name, article, price, url, count }) => {

	const dispatch = useDispatch();

	const handleDeleteCartProduct = () => {
		dispatch(deleteCartProduct(id));
		dispatch(setModalIsOpen(false));
	}

	const handleCloseModal = () => {
		dispatch(setModalIsOpen(false))
	}

	const handleOpenModal = () => {
		dispatch(setModalCloseBtn(true));
		dispatch(addModalTitle('Видалити товар з корзини'));
		dispatch(addModalBody(`Ви дійсно бажаєте видалити "${name}" з корзини?`));
		dispatch(addModalActions(
			<>
				<Button bgColor={'#158aa1'} onClick={handleDeleteCartProduct}>
					Видалити
				</Button>
				<Button bgColor={'#158aa1'} onClick={handleCloseModal}>
					Скасувати
				</Button>
			</>));
		dispatch(setModalIsOpen(true));
	}

	return (
		<tr className={styles.tableRow} key={id}>
			<td><img src={url} alt={name} width="150" height="150" /></td>
			<td className={styles.name}>{name} <p>Артикул: {article}</p> </td>
			<td>{price} грн</td>
			<td>{count}</td>
			<td>{price * count} грн</td>
			<td>
				<Button classes={styles.deleteBtn} onClick={handleOpenModal}>
					<DeleteIcon width={"18px"} height={"18px"} />
				</Button>
			</td>
		</tr>
	)
}

export default memo(CartItem);

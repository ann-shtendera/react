export const setDataToLocalStorage = (key, data) => {
	window.localStorage.setItem(key, JSON.stringify(data));
}

export const getDataFromLocalStorage = (key) => {
	const data = window.localStorage.getItem(key);

	if(data) {
		return JSON.parse(data);
	}
	return null;
}

export const removeDataFromLocalStorage = (key) => {
	window.localStorage.removeItem(key);
}
import React, { memo } from "react";

const CartIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="30"
      height="30"
      viewBox="0 0 256 256"
    >
      <g
        fill="#e8e8e8"
        strokeMiterlimit="10"
        strokeWidth="1"
        transform="matrix(2.81 0 0 2.81 1.407 1.407)"
      >
        <path d="M72.975 58.994h-41.12a3.5 3.5 0 01-3.347-2.477L15.199 13.006H3.5a3.5 3.5 0 110-7h14.289a3.5 3.5 0 013.347 2.476l13.309 43.512h36.204l10.585-25.191h-6.021a3.5 3.5 0 110-7H86.5a3.5 3.5 0 013.227 4.856L76.201 56.85a3.5 3.5 0 01-3.226 2.144z"></path>
        <circle cx="28.88" cy="74.33" r="6.16"></circle>
        <circle cx="74.59" cy="74.33" r="6.16"></circle>
        <path d="M62.278 26.547H35.197a3.5 3.5 0 110-7h27.081a3.5 3.5 0 110 7z"></path>
      </g>
    </svg>
  );
}

export default memo(CartIcon);
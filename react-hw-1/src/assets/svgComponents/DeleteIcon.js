import React, { memo } from 'react';
import PropTypes from 'prop-types';

const DeleteIcon = ({ width, height }) => {
	return (
		<svg
		xmlns="http://www.w3.org/2000/svg"
		width={width}
		height={height}
		viewBox="0 0 256 256"
	  >
		<g fill="#5c5c5c" strokeMiterlimit="10" strokeWidth="1">
		  <path
			d="M24.959 68.04a2.998 2.998 0 01-2.121-5.121l40.081-40.081a2.998 2.998 0 014.242 0 2.998 2.998 0 010 4.242l-40.08 40.081a2.992 2.992 0 01-2.122.879z"
			transform="matrix(2.81 0 0 2.81 1.407 1.407)"
		  ></path>
		  <path
			d="M65.04 68.04a2.99 2.99 0 01-2.121-.879l-40.081-40.08a2.998 2.998 0 010-4.242 2.998 2.998 0 014.242 0L67.161 62.92a2.998 2.998 0 01-2.121 5.12z"
			transform="matrix(2.81 0 0 2.81 1.407 1.407)"
		  ></path>
		  <path
			d="M45 90C20.187 90 0 69.813 0 45S20.187 0 45 0s45 20.187 45 45-20.187 45-45 45zm0-84C23.495 6 6 23.495 6 45s17.495 39 39 39 39-17.495 39-39S66.505 6 45 6z"
			transform="matrix(2.81 0 0 2.81 1.407 1.407)"
		  ></path>
		</g>
	  </svg>
	);
}

DeleteIcon.propTypes = {
	width: PropTypes.string, 
	height: PropTypes.string,
}

DeleteIcon.defaultProps = {
	width: "20px", 
	height: "20px",
}

export default memo(DeleteIcon);
